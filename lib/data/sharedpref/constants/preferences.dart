class Preferences {
  Preferences._();

  static const String isDarkMode = "is_dark_mode";
  static const String currentLanguage = "current_language";
  static const String authenticatedUser = "authenticated_user";
  static const String firebaseToken = "firebase_token";
  static const String accessToken = "access_token";
  static const String currentStepRegister = "current_step_register";
  static const String permissionCollectData = "permission_collect_data";
}
