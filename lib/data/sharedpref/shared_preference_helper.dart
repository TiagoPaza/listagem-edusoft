import 'package:shared_preferences/shared_preferences.dart';

import 'constants/preferences.dart';

class SharedPreferenceHelper {
  // shared pref instance
  final SharedPreferences _sharedPreference;

  // constructor
  SharedPreferenceHelper(this._sharedPreference);

  // General Methods: ----------------------------------------------------------
  Future<String?> get fcmToken async {
    return await this._sharedPreference.getString(Preferences.firebaseToken);
  }

  Future<bool> saveFcmToken(String fcmToken) async {
    return await this._sharedPreference.setString(Preferences.firebaseToken, fcmToken);
  }

  Future<bool> removeFcmToken() async {
    return await this._sharedPreference.remove(Preferences.firebaseToken);
  }

  Future<String?> get accessToken async {
    return await this._sharedPreference.getString(Preferences.accessToken);
  }

  Future<bool> saveAccessToken(String accessToken) async {
    return await this._sharedPreference.setString(Preferences.accessToken, accessToken);
  }

  Future<bool> removeAccessToken() async {
    return await this._sharedPreference.remove(Preferences.accessToken);
  }

  Future<String?> get authenticatedUser async {
    return await this._sharedPreference.getString(Preferences.authenticatedUser);
  }

  Future<bool> saveAuthenticatedUser(String authenticatedUser) async {
    return await this._sharedPreference.setString(Preferences.authenticatedUser, authenticatedUser);
  }

  Future<bool> removeAuthenticatedUser() async {
    return await this._sharedPreference.remove(Preferences.authenticatedUser);
  }

  Future<bool?> get permissionCollectData async {
    return await this._sharedPreference.getBool(Preferences.permissionCollectData);
  }

  Future<bool> savePermissionCollectData(bool permissionCollectData) async {
    return await this._sharedPreference.setBool(Preferences.permissionCollectData, permissionCollectData);
  }

  Future<bool> removePermissionCollectData() async {
    return await this._sharedPreference.remove(Preferences.permissionCollectData);
  }

  // // Theme:------------------------------------------------------
  // bool get isDarkMode {
  //   return _sharedPreference.getBool(Preferences.is_dark_mode) ?? false;
  // }
  //
  // Future<void> changeBrightnessToDark(bool value) {
  //   return _sharedPreference.setBool(Preferences.is_dark_mode, value);
  // }
  //
  // Language:---------------------------------------------------
  Future<String?> get currentLanguage async {
    return this._sharedPreference.getString(Preferences.currentLanguage);
  }

  Future<void> changeLanguage(String language) {
    return this._sharedPreference.setString(Preferences.currentLanguage, language);
  }
}
