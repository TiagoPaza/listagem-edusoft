import 'package:listagem_edusoft/data/network/errors/error.dart';

class NetworkError extends IError {
  NetworkError({required String message, required String description}) : super(message: message, description: description);
}
