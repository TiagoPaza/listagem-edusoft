import 'package:listagem_edusoft/data/network/errors/error.dart';

class InternalError extends IError {
  InternalError({required String message, required String description}) : super(message: message, description: description);
}
