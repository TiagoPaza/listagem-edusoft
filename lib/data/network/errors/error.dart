
abstract class IError implements Exception{
  String message, description;
  StackTrace? originalStack, stack;
  Exception? originalError;

  IError({
    required this.message,
    required this.description,
    this.originalError,
    this.originalStack,
    this.stack
  });

  void raise() => throw this;

}