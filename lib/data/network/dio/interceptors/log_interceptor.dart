import 'package:dio/dio.dart';

LogInterceptor logInterceptor = LogInterceptor(
  request: true,
  responseBody: true,
  requestBody: true,
  requestHeader: true,
);
      