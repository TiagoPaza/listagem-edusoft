import 'package:dio/dio.dart';
import 'package:listagem_edusoft/data/network/errors/error.dart';
import 'package:listagem_edusoft/data/network/errors/network_error.dart';

class DioErrorHandleHelper {
  // These errors will be handled in the same way.
  var timeoutSet = {DioErrorType.receiveTimeout, DioErrorType.connectTimeout, DioErrorType.sendTimeout, DioErrorType.cancel};

  // TODO: Define the developmentStrings for these Errors.
  IError handle(DioErrorType error) {
    if (timeoutSet.contains(error)) {
      return NetworkError(message: 'Lorem Ipsum', description: 'lorem Ipsum');
    } else if (error == DioErrorType.response) {
      return NetworkError(message: 'Lorem Ipsum', description: 'Lorem Ipsum');
    } else if (error == DioErrorType.other) {
      return NetworkError(message: 'Lorem Ipsum', description: 'Lorem Ipsum');
    }

    return NetworkError(message: 'Lorem Ipsum', description: 'Lorem Ipsum');
  }
}
