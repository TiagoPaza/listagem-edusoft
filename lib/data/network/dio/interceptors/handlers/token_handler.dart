import 'package:dio/dio.dart';

var tokenHandler = (RequestOptions options, RequestInterceptorHandler handler) async {
  print('onRequest Handler: Token');
  // To resolve with a custom data
  // return handler.resolve(Response(requestOptions:options,data:'fake data'));

  // Set a authorization token in the request Headers
  options.headers.putIfAbsent('Authorization', () =>  'KJW2csa_pa0jACdA6Cj9AS9Shg@23asf42F2dA23awr3a3@das');

  return handler.next(options);
};
