import 'package:dio/dio.dart';

var responseHandler = (Response response, ResponseInterceptorHandler handler) {
  /*
  Do something with response data, if you want to reject the request with a error message,
  you can reject a `DioError` object eg: `handler.reject(dioError) 
  */
  print('Response handler');

  if (response.data['success'] == false) {
    handler.reject(DioError(requestOptions: RequestOptions(path: ''), error: response.data['message']));
  }

  return handler.next(response); // continue
};
