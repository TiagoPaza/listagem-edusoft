import 'package:dio/dio.dart';
import 'package:listagem_edusoft/data/network/dio/interceptors/handlers/error_handler.dart';
import 'package:listagem_edusoft/data/network/dio/interceptors/handlers/response_handler.dart';
import 'package:listagem_edusoft/data/network/dio/interceptors/handlers/token_handler.dart';

InterceptorsWrapper oauthInterceptor = InterceptorsWrapper(
  onResponse: responseHandler,
  onRequest: tokenHandler,
  onError: errorHandler,
);
