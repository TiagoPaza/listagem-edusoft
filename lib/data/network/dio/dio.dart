import 'package:dio/dio.dart';
import 'package:listagem_edusoft/data/network/dio/interceptors/log_interceptor.dart';

class NetworkModule {
  Dio _dio;

  NetworkModule(this._dio) {
    this._dio
      ..options.connectTimeout = 15000
      ..options.receiveTimeout = 30000
      ..options.headers = {'Content-Type': 'application/json; charset=utf-8'}
      ..interceptors.addAll([
        logInterceptor,
        // oauthInterceptor,
      ])
      ..options.validateStatus = (status) => true;
  }

  Dio get dio => this._dio;
}
