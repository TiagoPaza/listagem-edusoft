import 'dart:async';
import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:get/get.dart';
import 'package:listagem_edusoft/constants/app_theme.dart';
import 'package:listagem_edusoft/constants/strings.dart';
import 'package:listagem_edusoft/routes.dart';
import 'package:listagem_edusoft/stores/language/language_store.dart';
import 'package:listagem_edusoft/ui/splash/screens/splash_layout.dart';
import 'package:permission_handler/permission_handler.dart';

import 'di/components/service_locator.dart';

// global instance for app component

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();

  await setPreferredOrientations();

  await setupLocator();

  if (Platform.isAndroid) {
    await Permission.storage;

    await Permission.microphone;
    await Permission.camera;

    await Permission.location;
    await Permission.locationAlways;
    await Permission.locationWhenInUse;

    await Permission.systemAlertWindow;
  }

  return runZonedGuarded(() async {
    runApp(ListagemEdusoftApp());
  }, (error, stack) {
    print(stack);
    print(error);
  });
}

Future<void> setPreferredOrientations() async {
  await SystemChrome.setPreferredOrientations([
    DeviceOrientation.portraitUp,
    DeviceOrientation.portraitDown,
    DeviceOrientation.landscapeRight,
    DeviceOrientation.landscapeLeft,
  ]);
}

class ListagemEdusoftApp extends StatelessWidget {
  final LanguageStore _languageStore = LanguageStore();

  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      debugShowCheckedModeBanner: false,
      title: Strings.appName,
      theme: themeData,
      onGenerateRoute: Routes.generateRoute,
      initialRoute: "/SplashLayout",
      home: SplashLayout(),
      locale: Locale(this._languageStore.locale),
      supportedLocales: this._languageStore.supportedLanguages.map((language) => Locale(language.locale!, language.code)).toList(),
      localizationsDelegates: [
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
        GlobalCupertinoLocalizations.delegate,
      ],
    );
  }
}
