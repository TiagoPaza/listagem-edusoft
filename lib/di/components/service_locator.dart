import 'package:dio/dio.dart';
import 'package:get_it/get_it.dart';
import 'package:listagem_edusoft/data/sharedpref/shared_preference_helper.dart';
import 'package:listagem_edusoft/di/modules/local_module.dart';
import 'package:listagem_edusoft/repositores/api/ibge/ibge_api_repository.dart';
import 'package:listagem_edusoft/repositores/api/ibge/ibge_api_service.dart';
import 'package:listagem_edusoft/ui/home/modals/more_information/bloc/more_information_bloc.dart';
import 'package:listagem_edusoft/ui/home/widgets/names_list/bloc/names_list_bloc.dart';
import 'package:shared_preferences/shared_preferences.dart';

final getIt = GetIt.instance;

Future<void> setupLocator() async {
  // async singletons:----------------------------------------------------------
  getIt.registerSingletonAsync<SharedPreferences>(() => LocalModule.provideSharedPreferences());

  // singletons:----------------------------------------------------------------
  getIt.registerSingleton(SharedPreferenceHelper(await getIt.getAsync<SharedPreferences>()));

  // services api's:---------------------------------------------------------------------
  getIt.registerSingleton(IBGEApiService(Dio()));

  // repositories:---------------------------------------------------------------------
  getIt.registerSingleton(IBGEApiRepository());

  // bloc:--------------------------------------------------------------------
  getIt.registerLazySingleton(() => NamesListBloc(ibgeApiRepository: getIt.get<IBGEApiRepository>()));
  getIt.registerLazySingleton(() => MoreInformationBloc(ibgeApiRepository: getIt.get<IBGEApiRepository>()));
}
