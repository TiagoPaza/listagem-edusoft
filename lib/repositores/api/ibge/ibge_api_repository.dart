import 'package:listagem_edusoft/di/components/service_locator.dart';
import 'package:listagem_edusoft/repositores/api/ibge/ibge_api_service.dart';

class IBGEApiRepository implements IBGEApiService {
  IBGEApiService _service = getIt.get<IBGEApiService>();

  @override
  Future<List<dynamic>?> get getNames async {
    return await this._service.getNames;
  }

  @override
  Future<List<dynamic>?> getNameDetails({required String name}) async {
    return await this._service.getNameDetails(name: name);
  }
}
