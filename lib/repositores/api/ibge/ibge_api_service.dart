import 'package:dio/dio.dart' as HttpDio;
import 'package:listagem_edusoft/data/network/dio/dio.dart';
import 'package:listagem_edusoft/data/network/errors/network_error.dart';

class IBGEApiService {
  // dio instance
  final HttpDio.Dio _dio;

  // injecting dio instance
  IBGEApiService(this._dio);

  Future<List<dynamic>?> get getNames async {
    NetworkModule networkModule = NetworkModule(this._dio);

    List<dynamic>? content;

    try {
      HttpDio.Response response = await networkModule.dio.get("https://servicodados.ibge.gov.br/api/v2/censos/nomes/");

      if (response.statusCode == 200 && response.data is List<dynamic>) {
        content = response.data;
      }
    } on HttpDio.DioError catch (dioError) {
      throw NetworkError(message: dioError.message, description: dioError.response!.data.toString());
    } catch (e) {
      print(e.toString());

      throw e;
    }

    return content;
  }

  Future<List<dynamic>?> getNameDetails({required String name}) async {
    NetworkModule networkModule = NetworkModule(this._dio);

    List<dynamic>? content;

    try {
      HttpDio.Response response = await networkModule.dio.get("https://servicodados.ibge.gov.br/api/v2/censos/nomes/$name");

      if (response.statusCode == 200 && response.data is List<dynamic>) {
        content = response.data;
      }
    } on HttpDio.DioError catch (dioError) {
      throw NetworkError(message: dioError.message, description: dioError.response!.data.toString());
    } catch (e) {
      print(e.toString());

      throw e;
    }

    return content;
  }
}
