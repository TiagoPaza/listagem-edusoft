import 'package:flutter/material.dart';
import 'package:listagem_edusoft/ui/home/home_layout.dart';
import 'package:listagem_edusoft/ui/splash/screens/splash_layout.dart';

class Routes {
  static Route<dynamic> generateRoute(RouteSettings settings) {
    // final args = settings.arguments;

    switch (settings.name) {
      case "/SplashLayout":
        return MaterialPageRoute(builder: (_) => SplashLayout());
      case "/HomeLayout":
        return MaterialPageRoute(builder: (_) => HomeLayout());
      default:
        // If there is no such named route in the switch statement, e.g. /third
        return MaterialPageRoute(builder: (_) => SplashLayout());
    }
  }
}
