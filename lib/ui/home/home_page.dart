import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_platform_widgets/flutter_platform_widgets.dart';
import 'package:listagem_edusoft/constants/app_theme.dart';
import 'package:listagem_edusoft/di/components/service_locator.dart';
import 'package:listagem_edusoft/models/name/name_model.dart';
import 'package:listagem_edusoft/ui/home/widgets/name_card.dart';
import 'package:listagem_edusoft/ui/home/widgets/names_list/bloc/names_list_bloc.dart';

class HomePage extends StatelessWidget {
  final ScrollController _scrollController;

  HomePage({required ScrollController scrollController, Key? key})
      : this._scrollController = scrollController,
        super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        gradient: LinearGradient(
          colors: [
            const Color(0xFF677ADB),
            themeData.primaryColor,
          ],
          stops: [0.0, 1.0],
        ),
      ),
      child: BlocBuilder<NamesListBloc, NamesListState>(
        bloc: getIt.get<NamesListBloc>(),
        builder: (BuildContext context, NamesListState state) {
          if (state.status == NamesListStatus.initial || state.status == NamesListStatus.failure) {
            getIt.get<NamesListBloc>().add(NamesListDataChanged());
          }

          if (state.status == NamesListStatus.success) {
            List<NameModel> nameList = state.data;

            return Container(
              padding: const EdgeInsets.all(10.0),
              child: nameList.length > 0
                  ? ListView.separated(
                      controller: this._scrollController,
                      itemCount: state.data.length,
                      itemBuilder: (BuildContext context, int index) {
                        NameModel nameModel = nameList[index];

                        return NameCard(nameModel: nameModel);
                      },
                      separatorBuilder: (BuildContext context, int index) {
                        return SizedBox(height: 10.0);
                      },
                    )
                  : const SizedBox.shrink(),
            );
          }

          return Center(
            child: PlatformCircularProgressIndicator(
              material: (_, __) => MaterialProgressIndicatorData(color: themeData.canvasColor),
              cupertino: (_, __) => CupertinoProgressIndicatorData(color: themeData.canvasColor),
            ),
          );
        },
      ),
    );
  }
}
