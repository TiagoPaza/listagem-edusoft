import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:listagem_edusoft/constants/app_theme.dart';
import 'package:listagem_edusoft/di/components/service_locator.dart';
import 'package:listagem_edusoft/models/name/name_model.dart';
import 'package:listagem_edusoft/ui/home/modals/more_information/bloc/more_information_bloc.dart';
import 'package:listagem_edusoft/ui/home/modals/name_details_modal.dart';
import 'package:listagem_edusoft/ui/home/widgets/names_list/bloc/names_list_bloc.dart';

class NameCard extends StatelessWidget {
  final NameModel _nameModel;

  NameCard({required NameModel nameModel, Key? key})
      : this._nameModel = nameModel,
        super(key: key);

  @override
  Widget build(BuildContext context) {
    return ClipRRect(
      borderRadius: const BorderRadius.all(Radius.circular(20.0)),
      child: Material(
        color: themeData.canvasColor.withOpacity(0.20),
        child: InkWell(
          child: Padding(
            padding: const EdgeInsets.all(20.0),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Expanded(
                  child: Text(
                    this._nameModel.nome!,
                    style: TextStyle(
                      color: themeData.canvasColor,
                      fontSize: 14.0,
                      fontWeight: FontWeight.w500,
                    ),
                  ),
                ),
                const SizedBox(width: 10.0),
                Text(
                  "${this._nameModel.rank}º",
                  style: TextStyle(
                    color: themeData.canvasColor,
                    fontSize: 20.0,
                  ),
                ),
              ],
            ),
          ),
          onTap: () async {
            getIt.get<MoreInformationBloc>().add(MoreInformationStatusChanged());

            await Get.to(() => NameDetailsModal(nameModel: this._nameModel));

            getIt.get<NamesListBloc>().add(NamesListDataChanged());
          },
        ),
      ),
    );
  }
}
