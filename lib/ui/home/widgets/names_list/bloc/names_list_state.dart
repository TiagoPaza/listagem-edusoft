part of 'names_list_bloc.dart';

enum NamesListStatus { initial, load, success, failure }

class NamesListState extends Equatable {
  final NamesListStatus status;
  final List<NameModel> data;

  const NamesListState({this.status = NamesListStatus.initial, this.data = const []});

  NamesListState copyWith({
    NamesListStatus? status,
    List<NameModel>? data,
  }) {
    return NamesListState(
      status: status ?? this.status,
      data: data ?? this.data,
    );
  }

  @override
  String toString() {
    return 'NamesListState{status: $status, data: $data}';
  }

  @override
  List<Object> get props => [status, data];
}
