part of 'names_list_bloc.dart';

abstract class NamesListEvent extends Equatable {
  const NamesListEvent();

  @override
  List<Object> get props => [];
}

class NamesListDataChanged extends NamesListEvent {
  const NamesListDataChanged();
}
