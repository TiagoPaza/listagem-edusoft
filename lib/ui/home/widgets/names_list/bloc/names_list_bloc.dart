import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:listagem_edusoft/models/name/name_model.dart';
import 'package:listagem_edusoft/repositores/api/ibge/ibge_api_repository.dart';

part 'names_list_event.dart';
part 'names_list_state.dart';

class NamesListBloc extends Bloc<NamesListEvent, NamesListState> {
  final IBGEApiRepository _ibgeApiRepository;

  NamesListBloc({
    required IBGEApiRepository ibgeApiRepository,
  })  : this._ibgeApiRepository = ibgeApiRepository,
        super(const NamesListState()) {
    on<NamesListDataChanged>(_onDataChanged);
  }

  void _onDataChanged(NamesListDataChanged event, Emitter<NamesListState> emit) async {
    emit(state.copyWith(status: NamesListStatus.load));

    List<dynamic>? response = await this._ibgeApiRepository.getNames;

    if (response != null) {
      List<NameModel> data = response.map<NameModel>((name) => NameModel.fromJson(name)).toList();

      emit(state.copyWith(data: data, status: NamesListStatus.success));
    } else {
      emit(state.copyWith(status: NamesListStatus.failure));
    }
  }
}
