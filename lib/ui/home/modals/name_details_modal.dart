import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_platform_widgets/flutter_platform_widgets.dart';
import 'package:get/get.dart';
import 'package:listagem_edusoft/constants/app_theme.dart';
import 'package:listagem_edusoft/di/components/service_locator.dart';
import 'package:listagem_edusoft/models/more_information/more_information_model.dart';
import 'package:listagem_edusoft/models/name/name_model.dart';
import 'package:listagem_edusoft/ui/home/modals/more_information/bloc/more_information_bloc.dart';

class NameDetailsModal extends StatelessWidget {
  final NameModel _nameModel;

  const NameDetailsModal({required NameModel nameModel, Key? key})
      : this._nameModel = nameModel,
        super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: themeData.backgroundColor,
      appBar: PreferredSize(
        preferredSize: const Size.fromHeight(56.0),
        child: _buildAppBar(),
      ),
      body: SafeArea(
        child: _buildMainContent(),
      ),
    );
  }

  Widget _buildAppBar() {
    return AppBar(
      elevation: 0.0,
      backgroundColor: themeData.backgroundColor,
      flexibleSpace: Container(
        decoration: BoxDecoration(
          gradient: LinearGradient(
            colors: [
              const Color(0xFF677ADB),
              themeData.primaryColor,
            ],
            stops: [0.0, 1.0],
          ),
        ),
      ),
      iconTheme: IconThemeData(
        color: themeData.canvasColor,
      ),
      systemOverlayStyle: SystemUiOverlayStyle(
        statusBarIconBrightness: Brightness.light, // For Android (light icons)
        statusBarBrightness: Brightness.dark, // For iOS (light icons)
        statusBarColor: themeData.primaryColor,
      ),
      title: Text(
        this._nameModel.nome != null ? this._nameModel.nome! : "",
      ),
    );
  }

  Widget _buildMainContent() {
    return Container(
      padding: const EdgeInsets.all(10.0),
      child: Column(
        children: <Widget>[
          ClipRRect(
            borderRadius: const BorderRadius.all(Radius.circular(20.0)),
            child: Container(
              width: Get.width,
              color: themeData.canvasColor,
              padding: const EdgeInsets.all(20.0),
              child: Column(
                children: <Widget>[
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Text("RANK:"),
                      const SizedBox(width: 10.0),
                      Expanded(
                        child: Text(
                          this._nameModel.rank != null ? this._nameModel.rank!.toString() : "",
                          style: TextStyle(
                            fontSize: 20.0,
                          ),
                        ),
                      ),
                    ],
                  ),
                  const SizedBox(height: 10.0),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Text("FREQUÊNCIA TOTAL:"),
                      const SizedBox(width: 10.0),
                      Expanded(
                        child: Text(
                          this._nameModel.freq != null ? this._nameModel.freq!.toString() : "",
                          style: TextStyle(
                            fontSize: 20.0,
                          ),
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ),
          const SizedBox(height: 10.0),
          Expanded(
            child: ClipRRect(
              borderRadius: const BorderRadius.all(Radius.circular(20.0)),
              child: Container(
                width: Get.width,
                color: themeData.canvasColor,
                padding: const EdgeInsets.all(20.0),
                child: BlocBuilder<MoreInformationBloc, MoreInformationState>(
                  bloc: getIt.get<MoreInformationBloc>(),
                  builder: (BuildContext context, MoreInformationState state) {
                    if (state.status == MoreInformationStatus.initial || state.status == MoreInformationStatus.failure) {
                      getIt
                          .get<MoreInformationBloc>()
                          .add(MoreInformationDataChanged(this._nameModel.nome != null ? this._nameModel.nome! : ""));
                    }

                    if (state.status == MoreInformationStatus.success) {
                      MoreInformationModel moreInformationModel = state.data.first;

                      return Column(
                        children: <Widget>[
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Text("LOCALIDADE:"),
                              const SizedBox(width: 10.0),
                              Expanded(
                                child: Text(
                                  moreInformationModel.localidade != null ? moreInformationModel.localidade! : "",
                                  style: TextStyle(
                                    fontSize: 20.0,
                                  ),
                                ),
                              ),
                            ],
                          ),
                          const SizedBox(height: 10.0),
                          if (moreInformationModel.res != null)
                            Expanded(
                              child: ListView.separated(
                                itemCount: moreInformationModel.res!.length,
                                itemBuilder: (BuildContext context, int index) {
                                  MoreInformationModelRes res = moreInformationModel.res![index];
                                  List<String> periodSplit = [];

                                  if (res.periodo != null) {
                                    periodSplit = res.periodo!.split("[");
                                  }

                                  return Container(
                                    child: Column(
                                      mainAxisAlignment: MainAxisAlignment.center,
                                      children: <Widget>[
                                        Row(
                                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                          children: <Widget>[
                                            Text("PERÍODO:"),
                                            const SizedBox(width: 10.0),
                                            if (periodSplit.length > 2)
                                              Expanded(
                                                child: Text(
                                                  periodSplit[1],
                                                  style: TextStyle(
                                                    fontSize: 20.0,
                                                  ),
                                                ),
                                              )
                                            else
                                              Expanded(
                                                child: Text(
                                                  periodSplit[0],
                                                  style: TextStyle(
                                                    fontSize: 20.0,
                                                  ),
                                                ),
                                              )
                                          ],
                                        ),
                                        Row(
                                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                          children: <Widget>[
                                            Text("FREQUÊNCIA:"),
                                            const SizedBox(width: 10.0),
                                            Expanded(
                                              child: Text(
                                                res.frequencia != null ? res.frequencia!.toString() : "",
                                                style: TextStyle(
                                                  fontSize: 20.0,
                                                ),
                                              ),
                                            ),
                                          ],
                                        ),
                                      ],
                                    ),
                                  );
                                },
                                separatorBuilder: (BuildContext context, int index) {
                                  return Divider();
                                },
                              ),
                            ),
                        ],
                      );
                    }

                    return Center(
                      child: PlatformCircularProgressIndicator(
                        material: (_, __) => MaterialProgressIndicatorData(color: themeData.canvasColor),
                        cupertino: (_, __) => CupertinoProgressIndicatorData(color: themeData.canvasColor),
                      ),
                    );
                  },
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
