import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:listagem_edusoft/models/more_information/more_information_model.dart';
import 'package:listagem_edusoft/repositores/api/ibge/ibge_api_repository.dart';

part 'more_information_event.dart';
part 'more_information_state.dart';

class MoreInformationBloc extends Bloc<MoreInformationEvent, MoreInformationState> {
  final IBGEApiRepository _ibgeApiRepository;

  MoreInformationBloc({
    required IBGEApiRepository ibgeApiRepository,
  })  : this._ibgeApiRepository = ibgeApiRepository,
        super(const MoreInformationState()) {
    on<MoreInformationDataChanged>(_onDataChanged);
    on<MoreInformationStatusChanged>(_onStatusChanged);
  }

  void _onStatusChanged(MoreInformationStatusChanged event, Emitter<MoreInformationState> emit) async {
    emit(state.copyWith(name: "", status: MoreInformationStatus.initial));
  }

  void _onDataChanged(MoreInformationDataChanged event, Emitter<MoreInformationState> emit) async {
    emit(state.copyWith(name: event.name, status: MoreInformationStatus.load));

    print("state.name: ${state.name}");

    List<dynamic>? response = await this._ibgeApiRepository.getNameDetails(name: state.name);

    if (response != null) {
      List<MoreInformationModel> data = response.map<MoreInformationModel>((name) => MoreInformationModel.fromJson(name)).toList();

      emit(state.copyWith(data: data, status: MoreInformationStatus.success));
    } else {
      emit(state.copyWith(status: MoreInformationStatus.failure));
    }
  }
}
