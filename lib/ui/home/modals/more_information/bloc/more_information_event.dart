part of 'more_information_bloc.dart';

abstract class MoreInformationEvent extends Equatable {
  const MoreInformationEvent();

  @override
  List<Object> get props => [];
}

class MoreInformationStatusChanged extends MoreInformationEvent {
  const MoreInformationStatusChanged();
}

class MoreInformationDataChanged extends MoreInformationEvent {
  final String name;

  const MoreInformationDataChanged(this.name);

  @override
  List<Object> get props => [name];
}
