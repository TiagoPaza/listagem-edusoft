part of 'more_information_bloc.dart';

enum MoreInformationStatus { initial, load, success, failure }

class MoreInformationState extends Equatable {
  final MoreInformationStatus status;
  final String name;
  final List<MoreInformationModel> data;

  const MoreInformationState({
    this.status = MoreInformationStatus.initial,
    this.name = "",
    this.data = const [],
  });

  MoreInformationState copyWith({
    MoreInformationStatus? status,
    String? name,
    List<MoreInformationModel>? data,
  }) {
    return MoreInformationState(
      status: status ?? this.status,
      name: name ?? this.name,
      data: data ?? this.data,
    );
  }

  @override
  String toString() {
    return 'MoreInformationState{status: $status, name: $name, data: $data}';
  }

  @override
  List<Object> get props => [status, name, data];
}
