import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:listagem_edusoft/constants/app_theme.dart';
import 'package:listagem_edusoft/ui/home/home_page.dart';
import 'package:scroll_app_bar/scroll_app_bar.dart';

class HomeLayout extends StatefulWidget {
  HomeLayout({Key? key}) : super(key: key);

  @override
  _HomeLayoutState createState() => _HomeLayoutState();
}

class _HomeLayoutState extends State<HomeLayout> {
  final ScrollController _scrollController = ScrollController();

  @override
  void initState() {
    super.initState();

    WidgetsBinding.instance?.addPostFrameCallback((_) async {
      if (!mounted) return;
      setState(() {});
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: themeData.backgroundColor,
      appBar: PreferredSize(
        preferredSize: const Size.fromHeight(56.0),
        child: _buildAppBar(),
      ),
      body: SafeArea(
        child: HomePage(scrollController: this._scrollController),
      ),
    );
  }

  Widget _buildAppBar() {
    return ScrollAppBar(
      elevation: 0.0,
      backgroundColor: themeData.backgroundColor,
      controller: this._scrollController,
      backgroundGradient: LinearGradient(
        colors: [
          const Color(0xFF677ADB),
          themeData.primaryColor,
        ],
        stops: [0.0, 1.0],
      ),
      iconTheme: IconThemeData(
        color: themeData.canvasColor,
      ),
      systemOverlayStyle: SystemUiOverlayStyle(
        statusBarIconBrightness: Brightness.light, // For Android (light icons)
        statusBarBrightness: Brightness.dark, // For iOS (light icons)
        statusBarColor: themeData.primaryColor,
      ),
    );
  }
}
