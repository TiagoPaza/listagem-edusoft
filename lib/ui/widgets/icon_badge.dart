import 'package:flutter/material.dart';

class IconBadge extends StatelessWidget {
  final Widget child;
  final Color color;
  final int count;
  final double size;

  IconBadge({Key? key, required this.child, required this.color, required this.count, this.size = 24.0}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        this.child,
        if (this.count != 0)
          Positioned(
            right: 0.0,
            child: Container(
              width: 12.0,
              height: 12.0,
              decoration: BoxDecoration(
                color: this.color.withOpacity(0.25), // border color
                shape: BoxShape.circle,
              ),
              child: Padding(
                padding: EdgeInsets.all(2), // border width
                child: Container(
                  // or ClipRRect if you need to clip the content
                  decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    color: this.color, // inner circle color
                  ),
                  child: Container(), // inner content
                ),
              ),
            ),
          )
      ],
    );
  }
}
