import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:listagem_edusoft/constants/app_theme.dart';

class PrimaryButton extends StatelessWidget {
  final String _text;
  final VoidCallback? _onPressed;
  final Color? _backgroundColor;

  const PrimaryButton({required String text, required VoidCallback? onPressed, Color? backgroundColor, Key? key})
      : this._text = text,
        this._onPressed = onPressed,
        this._backgroundColor = backgroundColor,
        super(key: key);

  @override
  Widget build(BuildContext context) {
    return ElevatedButton(
      key: key,
      style: ButtonStyle(
        minimumSize: MaterialStateProperty.all<Size>(Size(Get.width, 40.0)),
        backgroundColor: MaterialStateProperty.resolveWith<Color>(
          (states) {
            if (states.contains(MaterialState.disabled)) {
              return this._backgroundColor != null ? this._backgroundColor!.withOpacity(0.60) : themeData.primaryColor.withOpacity(0.60);
            }

            return this._backgroundColor != null ? this._backgroundColor! : themeData.primaryColor;
          },
        ),
        shape: MaterialStateProperty.all<RoundedRectangleBorder>(
          RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(40.0),
            side: BorderSide(color: this._backgroundColor != null ? this._backgroundColor! : themeData.primaryColor),
          ),
        ),
      ),
      child: Padding(
        padding: EdgeInsets.fromLTRB(10.0, 20.0, 10.0, 20.0),
        child: Text(this._text),
      ),
      onPressed: this._onPressed,
    );
  }
}
