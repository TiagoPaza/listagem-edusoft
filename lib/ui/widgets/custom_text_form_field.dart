import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:listagem_edusoft/constants/app_theme.dart';

class CustomTextFormField extends StatelessWidget {
  final TextEditingController? controller;
  final bool readOnly;
  final TextInputType keyboardType;
  final TextInputAction textInputAction;
  final AutovalidateMode autovalidateMode;
  final TextCapitalization textCapitalization;
  final String? labelText;
  final Widget? prefixIcon;
  final Widget? suffixIcon;
  final int? maxLines;
  final FormFieldValidator<String>? validator;
  final ValueChanged<String>? onChanged;
  final GestureTapCallback? onTap;
  final bool obscureText;
  final String? errorText;
  final List<TextInputFormatter>? inputFormatters;

  const CustomTextFormField({
    Key? key,
    this.controller,
    this.readOnly = false,
    this.keyboardType = TextInputType.text,
    this.textInputAction = TextInputAction.next,
    this.autovalidateMode = AutovalidateMode.onUserInteraction,
    this.textCapitalization = TextCapitalization.sentences,
    this.labelText,
    this.prefixIcon,
    this.suffixIcon,
    this.maxLines = 1,
    this.validator,
    this.onChanged,
    this.onTap,
    this.obscureText = false,
    this.errorText,
    this.inputFormatters,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      readOnly: this.readOnly,
      controller: this.controller,
      keyboardType: this.keyboardType,
      textInputAction: this.textInputAction,
      autovalidateMode: this.autovalidateMode,
      textCapitalization: this.textCapitalization,
      validator: this.validator,
      onChanged: this.onChanged,
      onTap: this.onTap,
      obscureText: this.obscureText,
      maxLines: this.maxLines,
      style: TextStyle(color: themeData.primaryColor),
      decoration: InputDecoration(
        filled: true,
        fillColor: themeData.canvasColor,
        labelText: this.labelText,
        labelStyle: TextStyle(color: const Color(0xFF343434)),
        prefixIcon: this.prefixIcon,
        suffixIcon: this.suffixIcon,
        focusedBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(20.0),
          borderSide: BorderSide(
            color: const Color(0xFF000000).withOpacity(0.30),
          ),
        ),
        enabledBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(40.0),
          borderSide: BorderSide(
            color: const Color(0xFF000000).withOpacity(0.30),
            width: 1.0,
          ),
        ),
        errorBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(40.0),
          borderSide: BorderSide(
            color: Colors.red.withOpacity(0.30),
            width: 1.0,
          ),
        ),
        focusedErrorBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(40.0),
          borderSide: BorderSide(
            color: Colors.red.withOpacity(0.30),
            width: 1.0,
          ),
        ),
        contentPadding: EdgeInsets.fromLTRB(20.0, 5.0, 20.0, 5.0),
        errorText: this.errorText,
      ),
      inputFormatters: this.inputFormatters,
    );
  }
}
