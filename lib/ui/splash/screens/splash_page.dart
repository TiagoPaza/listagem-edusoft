import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:listagem_edusoft/constants/app_theme.dart';

class SplashPage extends StatelessWidget {
  const SplashPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: Get.height,
      width: Get.width,
      decoration: BoxDecoration(
        gradient: LinearGradient(
          colors: [
            const Color(0xFF677ADB),
            themeData.primaryColor,
          ],
          stops: [0.0, 1.0],
        ),
      ),
    );
  }
}
