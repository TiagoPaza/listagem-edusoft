import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:listagem_edusoft/constants/app_theme.dart';
import 'package:listagem_edusoft/ui/home/home_layout.dart';
import 'package:listagem_edusoft/ui/splash/screens/splash_page.dart';

class SplashLayout extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _SplashLayoutState();
}

class _SplashLayoutState extends State<SplashLayout> {
  @override
  void initState() {
    WidgetsBinding.instance?.addPostFrameCallback((_) async {
      await Future.delayed(const Duration(milliseconds: 1500), () async {
        await Get.offAll(() => HomeLayout());
      });
    });

    super.initState();
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: themeData.backgroundColor,
      appBar: PreferredSize(
        preferredSize: const Size.fromHeight(56.0),
        child: _buildAppBar(),
      ),
      body: SafeArea(
        child: SplashPage(),
      ),
    );
  }

  Widget _buildAppBar() {
    return AppBar(
      elevation: 0.0,
      backgroundColor: themeData.backgroundColor,
      flexibleSpace: Container(
        decoration: BoxDecoration(
          gradient: LinearGradient(
            colors: [
              const Color(0xFF677ADB),
              themeData.primaryColor,
            ],
            stops: [0.0, 1.0],
          ),
        ),
      ),
      iconTheme: IconThemeData(
        color: themeData.canvasColor,
      ),
      systemOverlayStyle: SystemUiOverlayStyle(
        statusBarIconBrightness: Brightness.light, // For Android (light icons)
        statusBarBrightness: Brightness.dark, // For iOS (light icons)
        statusBarColor: themeData.primaryColor,
      ),
      automaticallyImplyLeading: false,
    );
  }
}
