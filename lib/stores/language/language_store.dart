import 'package:listagem_edusoft/models/language/language_model.dart';
import 'package:mobx/mobx.dart';

part 'language_store.g.dart';

class LanguageStore = _LanguageStore with _$LanguageStore;

abstract class _LanguageStore with Store {
  // supported languages
  List<LanguageModel> supportedLanguages = [
    LanguageModel(code: "US", locale: "en", language: "English"),
    LanguageModel(code: "BR", locale: "pt", language: "Português(Brasil)"),
    LanguageModel(code: "ES", locale: "es", language: "Español"),
  ];

  // constructor:---------------------------------------------------------------
  _LanguageStore() {
    init();
  }

  // store variables:-----------------------------------------------------------
  @observable
  String _locale = "pt";

  @observable
  Map<String, dynamic>? localizedStrings;

  @computed
  String get locale => this._locale;

  // actions:-------------------------------------------------------------------
  @action
  void changeLanguage(String value) {
    this._locale = value;

    // this._repository.changeLanguage(value);

    // loadJSON();
  }

  @action
  String getCode() {
    String code = "";

    if (this._locale == "en") {
      code = "US";
    } else if (this._locale == "pt") {
      code = "BR";
    } else if (this._locale == "es") {
      code = "ES";
    }

    return code;
  }

  @action
  String? getLanguage() {
    return supportedLanguages[supportedLanguages.indexWhere((language) => language.locale == this._locale)].language;
  }

  @action
  String translate(String key) {
    var translation = _getTranslation(key, localizedStrings!);

    return translation;
    // return translation ?? key;
  }

  String _getTranslation(String key, Map<String, dynamic> map) {
    List<String> keys = key.split('.');

    if (keys.length > 1) {
      var firstKey = keys.first;

      if (map.containsKey(firstKey) && map[firstKey] is! String) {
        return _getTranslation(key.substring(key.indexOf('.') + 1), map[firstKey]);
      }
    }

    return map[key];
  }

  // general:-------------------------------------------------------------------
  void init() async {
    // getting current language from shared preference
    // this._repository.currentLanguage.then((locale) {
    //   if (locale != null && locale.isNotEmpty) {
    //     this._locale = locale;
    //   }
    //
    //   // loadJSON();
    // });
  }

  void loadJSON() {
    // rootBundle.loadString('assets/i18n/${this._locale}.json').then((value) {
    //   Map<String, dynamic> jsonMap = json.decode(value);
    //
    //   localizedStrings = jsonMap;
    // });
  }

  // dispose:-------------------------------------------------------------------
  dispose() {}
}
