// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'language_store.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$LanguageStore on _LanguageStore, Store {
  Computed<String>? _$localeComputed;

  @override
  String get locale => (_$localeComputed ??=
          Computed<String>(() => super.locale, name: '_LanguageStore.locale'))
      .value;

  final _$_localeAtom = Atom(name: '_LanguageStore._locale');

  @override
  String get _locale {
    _$_localeAtom.reportRead();
    return super._locale;
  }

  @override
  set _locale(String value) {
    _$_localeAtom.reportWrite(value, super._locale, () {
      super._locale = value;
    });
  }

  final _$localizedStringsAtom = Atom(name: '_LanguageStore.localizedStrings');

  @override
  Map<String, dynamic>? get localizedStrings {
    _$localizedStringsAtom.reportRead();
    return super.localizedStrings;
  }

  @override
  set localizedStrings(Map<String, dynamic>? value) {
    _$localizedStringsAtom.reportWrite(value, super.localizedStrings, () {
      super.localizedStrings = value;
    });
  }

  final _$_LanguageStoreActionController =
      ActionController(name: '_LanguageStore');

  @override
  void changeLanguage(String value) {
    final _$actionInfo = _$_LanguageStoreActionController.startAction(
        name: '_LanguageStore.changeLanguage');
    try {
      return super.changeLanguage(value);
    } finally {
      _$_LanguageStoreActionController.endAction(_$actionInfo);
    }
  }

  @override
  String getCode() {
    final _$actionInfo = _$_LanguageStoreActionController.startAction(
        name: '_LanguageStore.getCode');
    try {
      return super.getCode();
    } finally {
      _$_LanguageStoreActionController.endAction(_$actionInfo);
    }
  }

  @override
  String? getLanguage() {
    final _$actionInfo = _$_LanguageStoreActionController.startAction(
        name: '_LanguageStore.getLanguage');
    try {
      return super.getLanguage();
    } finally {
      _$_LanguageStoreActionController.endAction(_$actionInfo);
    }
  }

  @override
  String translate(String key) {
    final _$actionInfo = _$_LanguageStoreActionController.startAction(
        name: '_LanguageStore.translate');
    try {
      return super.translate(key);
    } finally {
      _$_LanguageStoreActionController.endAction(_$actionInfo);
    }
  }

  @override
  String toString() {
    return '''
localizedStrings: ${localizedStrings},
locale: ${locale}
    ''';
  }
}
