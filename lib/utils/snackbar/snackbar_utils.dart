import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:listagem_edusoft/constants/app_theme.dart';

class SnackbarUtils {
  static Future<void> success({required String description}) async {
    await Get.snackbar(
      "Sucesso! Sua ação foi concluída.",
      description,
      duration: const Duration(seconds: 5),
      barBlur: 5.0,
      backgroundColor: themeData.canvasColor,
      colorText: themeData.primaryColor,
      leftBarIndicatorColor: Colors.green,
      borderRadius: 10.0,
      snackPosition: SnackPosition.TOP,
    );
  }

  static Future<void> error({required String description}) async {
    await Get.snackbar(
      "Ops! Houve um problema com a sua ação.",
      description,
      duration: const Duration(seconds: 5),
      barBlur: 5.0,
      backgroundColor: themeData.canvasColor,
      colorText: themeData.primaryColor,
      leftBarIndicatorColor: Colors.red,
      borderRadius: 10.0,
      snackPosition: SnackPosition.TOP,
    );
  }
}
