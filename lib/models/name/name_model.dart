class NameModel {
  String? nome;
  int? regiao;
  int? freq;
  int? rank;
  String? sexo;

  NameModel({this.nome, this.regiao, this.freq, this.rank, this.sexo});

  NameModel.fromJson(Map<String, dynamic> json) {
    nome = json['nome'];
    regiao = json['regiao'];
    freq = json['freq'];
    rank = json['rank'];
    sexo = json['sexo'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();

    data['nome'] = this.nome;
    data['regiao'] = this.regiao;
    data['freq'] = this.freq;
    data['rank'] = this.rank;
    data['sexo'] = this.sexo;

    return data;
  }
}
