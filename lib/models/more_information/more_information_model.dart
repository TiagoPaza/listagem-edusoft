class MoreInformationModel {
  String? nome;
  String? sexo;
  String? localidade;
  List<MoreInformationModelRes>? res;

  MoreInformationModel({this.nome, this.sexo, this.localidade, this.res});

  MoreInformationModel.fromJson(Map<String, dynamic> json) {
    nome = json['nome'];
    sexo = json['sexo'];
    localidade = json['localidade'];

    if (json['res'] != null) {
      res = <MoreInformationModelRes>[];

      json['res'].forEach((v) {
        res!.add(new MoreInformationModelRes.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();

    data['nome'] = this.nome;
    data['sexo'] = this.sexo;
    data['localidade'] = this.localidade;

    if (this.res != null) {
      data['res'] = this.res!.map((v) => v.toJson()).toList();
    }

    return data;
  }
}

class MoreInformationModelRes {
  String? periodo;
  int? frequencia;

  MoreInformationModelRes({this.periodo, this.frequencia});

  MoreInformationModelRes.fromJson(Map<String, dynamic> json) {
    periodo = json['periodo'];
    frequencia = json['frequencia'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();

    data['periodo'] = this.periodo;
    data['frequencia'] = this.frequencia;

    return data;
  }
}
