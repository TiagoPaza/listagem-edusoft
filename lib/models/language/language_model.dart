class LanguageModel {
  String? code;
  String? locale;
  String? language;
  Map<String, String>? dictionary;

  LanguageModel({this.code, this.locale, this.language, this.dictionary});
}
