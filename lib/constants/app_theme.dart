/// Creating custom color palettes is part of creating a custom app. The idea is to create
/// your class of custom colors, in this case `CompanyColors` and then create a `ThemeData`
/// object with those colors you just defined.
///
/// Resource:
/// A good resource would be this website: http://mcg.mbitson.com/
/// You simply need to put in the colour you wish to use, and it will generate all shades
/// for you. Your primary colour will be the `500` value.
///
/// Colour Creation:
/// In order to create the custom colours you need to create a `Map<int, Color>` object
/// which will have all the shade values. `const Color(0xFF...)` will be how you create
/// the colours. The six character hex code is what follows. If you wanted the colour
/// #114488 or #D39090 as primary colours in your theme, then you would have
/// `const Color(0x114488)` and `const Color(0xD39090)`, respectively.
///
/// Usage:
/// In order to use this newly created theme or even the colours in it, you would just
/// `import` this file in your project, anywhere you needed it.
/// `import 'path/to/theme.dart';`
import 'package:flutter/material.dart';
import 'package:listagem_edusoft/constants/font_family.dart';

final ThemeData themeData = ThemeData(
  brightness: Brightness.light,
  fontFamily: FontFamily.roboto,
  primaryColor: const Color(0xFF2B335C),
  secondaryHeaderColor: const Color(0xFFADB6E1),
  accentColor: const Color(0xFF394061),
  // colorScheme: ColorScheme(
  //   secondary: const Color(0xFF566268),
  // ),
  backgroundColor: const Color(0xFFADB6E1),
  canvasColor: const Color(0xFFFFFFFF),
  cardColor: const Color(0xFFFFFFFF),
  visualDensity: VisualDensity.adaptivePlatformDensity,
);
